import { Component, OnInit } from '@angular/core';
import{AuthService} from '../auth.service';
import { Router } from '@angular/router';
import { BooksService } from '../books.service';
import { AngularFireDatabase } from '@angular/fire/database';
@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books=[];
  bookName:string;
  writerName:string;
  readStatus= false;

  user=[];
  nickname:string;

  toLogin(){
    this.router.navigate(['/login']);
  }

  toRegister(){
    this.router.navigate(['/register']);
  }

  addBooks(){
      this.booksService.addBook(this.bookName,this.writerName);
      this.bookName = '';
      this.writerName = '';
      this.readStatus = false;
  }


  constructor(public authService:AuthService,private router:Router, private booksService:BooksService, private db:AngularFireDatabase) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/details').snapshotChanges().subscribe(
        details => {
          details.forEach(
                detail => {
                  let y = detail.payload.toJSON();
                  this.user.push(y);            
                   this.nickname = this.user[0].nickname;   
                   console.log(this.nickname);      
            }
          )
          
         
        }
      )
  }
)
    
    this.authService.user.subscribe(user => {
      this.db.list('/users/' + user.uid + '/books').snapshotChanges().subscribe(
        books => {
          this.books = [];
          books.forEach(
            book => {
              let y = book.payload.toJSON();
              y["key"] = book.key;
              this.books.push(y);
            }
          ); console.log(this.books);
        }
      )
    })
  }
}
