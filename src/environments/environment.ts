// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBgN0uTdmg3HOeOt65MPzXC0uHM7lnmPZE",
    authDomain: "exam-e37c4.firebaseapp.com",
    databaseURL: "https://exam-e37c4.firebaseio.com",
    projectId: "exam-e37c4",
    storageBucket: "exam-e37c4.appspot.com",
    messagingSenderId: "374183718091"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
