import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  
  toLogin(){
    this.router.navigate(['/login']);
  }

  toRegister(){
    this.router.navigate(['/register']);
  }

  toBooks(){
    this.router.navigate(['/books']);
  }

  toLogout(){
    this.authService.logout().
    then(value =>{
    this.router.navigate(['/login'])
    }).catch(err=> {
      console.log(err)
    })
  }

  constructor(private router:Router, public authService:AuthService, private db:AngularFireDatabase) { }

  ngOnInit() {
   
    
}
}
