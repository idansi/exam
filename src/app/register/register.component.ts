import { Component, OnInit } from '@angular/core';
import{AuthService} from '../auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email:string;
  password:string;
  rePassword:string;
  name:string;
  userName:string;
  code= '';
  message= '';
  required:string;
  passwordMessage:string;
  passwordCheck= false;
  require=true;
 hide=true;
  toRegister(){

    this.require=true;

    if ( this.password == null || this.email == null || this.name == null || this.userName == null) 
    {
      this.required = "this input is required";
      this.require = false;
    }
    
    if(this.password == this.rePassword){
      this.passwordCheck = true;
    }
    if (this.require && this.passwordCheck)
    {
      this.authService.register(this.email,this.password)
      .then(value => { 
        this.authService.updateProfile(value.user, this.userName);
        this.authService.addUser(value.user, this.userName);
      }).then(value => {
        this.router.navigate(['/books']);
      }).catch(err => {
        this.code = err.code;
        this.message = err.message;
      })
    }

    else{
      this.passwordMessage= ' The passwords do not equal '
    }


  
  }

   
  
  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

}

